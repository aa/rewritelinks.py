import argparse, html5lib, sys, os
from urllib.parse import urlparse
from xml.etree import ElementTree as ET
# http://etherbox.local/pad/p/Wednesday -> Wednesday.diff.html
# http://etherbox.lan/pad/p/database_anxiety -> database_anxiety.diff.html
# fix href's that end with )


def process_file (n):
    print (n)
    changed = False
    with open(n) as f:
        t = html5lib.parse(f, namespaceHTMLElements=False)
        for link in t.findall(".//a[@href]"):
            href = link.attrib.get("href")
            # print (link, href)
            if href.endswith(")"):
                print ("  Trimming trailing ) in a.href: {0}".format(href), file=sys.stderr)
                link.attrib['href'] = href.rstrip(")")
                changed = True
            if href.startswith("http://etherbox."):
                # href = urljoin("http://etherbox.local", href)
                path = urlparse(href).path
                if path.startswith("/pad/p/"):
                    newhref = path.split("/")[-1] + ".diff.html"
                else:
                    newhref = os.path.relpath(path, "/etherdump")
                link.attrib['href'] = newhref
                print ("  Mapping local link {0} -> {1}".format(href, newhref), file=sys.stderr)
                changed=True
    if changed:
        with open(n, "w") as f:
            print (ET.tostring(t, method="html", encoding="unicode"), file=f)
        # for link in t.findall(".//*[@src]"):
        #     href = link.attrib.get("src")
        #     print (link, href)

ap = argparse.ArgumentParser("")
ap.add_argument("input", nargs="+")
args = ap.parse_args()

for n in args.input:
    process_file(n)
